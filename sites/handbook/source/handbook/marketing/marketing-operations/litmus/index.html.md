---
layout: handbook-page-toc
title: "Litmus"
description: "Litmus is email marketing software used primarily by marketing campaign managers to build, test, and optimize our email marketing programs."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Uses

Litmus is email marketing software used primarily by marketing campaign managers to build, test, and optimize our email marketing programs. 

## Litmus Analytics

In order to avoid collecting PII data while using Litmus Analytics, please follow the instructions below to add the tracking script to your email in Marketo:

1. Select `Other` as your ESP.
1. For the merge tag field, use `{{lead.Id}}`. 