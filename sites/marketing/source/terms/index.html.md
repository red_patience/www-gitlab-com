---
layout: markdown_page
title: GitLab Terms of Service
description: "Here you can find information on the terms related to GitLab's Website and your use of GitLab Software"
canonical_path: "/terms/"
---
Thank you for choosing GitLab! For the current terms please see the Current Terms of Use table below. 

If you received an Order Form / Quote, or purchased, prior to February 1, 2021, please see the Agreement History below for the terms applicable to your purchase and/or use of GitLab software.

## **CURRENT TERMS OF USE**

| Type of Use / Activity  | Applicable Agreement |  
| ------ | ------ |  
| Use of any [GitLab Software Offering](https://about.gitlab.com/pricing/), including Free tier | [Subscription Agreement](https://about.gitlab.com/handbook/legal/subscription-agreement/) |  
| Use of any GitLab Professional Services | [Professional Services Agreement](https://about.gitlab.com/handbook/legal/professional-services-agreement/) |  
| Privacy Policy | [GitLab Privacy Policy](https://about.gitlab.com/privacy/) |  
| GitLab Processing Your Data | [GitLab Data Processing Agreement](https://about.gitlab.com/handbook/legal/data-processing-agreement/) |  
| Request Removal of Content / Data | [DMCA Notice and Take Down](https://about.gitlab.com/handbook/dmca/) |  
| Use of GitLab's Website(s)  | [Website Terms of Use](https://about.gitlab.com/handbook/legal/policies/website-terms-of-use/) |  
| Cookies & Visiting the Website | [GitLab Cookie Policy](https://about.gitlab.com/privacy/cookies/) |  
| Partner Program | Enroll and Terms please visit the [GitLab Partner Program](https://partners.gitlab.com/English/) | 
| GitLab for Education Program* | [GitLab for Education Program Agreement](https://about.gitlab.com/handbook/legal/education-agreement/) |
| GitLab for Open Source Program** | [GitLab for Open Source Program Agreement](https://about.gitlab.com/handbook/legal/opensource-agreement/) |  

*Only applicable for Educational Institutions using GitLab Software for Instructional Use, or, Non-Commercial Academic Research. Please visit [Program Guidelines](https://about.gitlab.com/solutions/education/join/) for more information.

**Only applicable for Open Source Organizations using GitLab Software for Open Source Software. Please visit [Program Guidelines](https://about.gitlab.com/solutions/open-source/join/) for more information.


## **AGREEMENT HISTORY**  

GitLab, including GitLab Legal, is committed to [transparency](https://about.gitlab.com/handbook/values/#transparency) as part of its’ values, as such we provide previous versions of our Agreements.   

| Legacy Agreement and Applicable Dates | Agreement Location |  
| ------ | ------ |  
| Subscription Agreement (January 2015 - January 2021) | [Legacy Subscription Agreement](https://about.gitlab.com/terms/signature.html) |  
