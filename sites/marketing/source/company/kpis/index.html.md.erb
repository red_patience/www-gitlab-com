---
layout: handbook-page-toc
title: KPIs
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What are KPIs

Every part of GitLab has Key Performance Indicators (KPIs) linked to the company [OKRs](/company/okrs/).
Avoid the term metric where we can be more explicit.
Use KPI instead.
A function's KPIs are owned by the respective member of e-group.
A function may have many performance indicators (PIs) they track and not all of them will be KPIs.
KPIs should be a subset of PIs and used to indicate the most important PIs to be surfaced to leadership.

The KPI definition should be in the most relevant part of the handbook which is organized by [function and results](/handbook/handbook-usage/#style-guide-and-information-architecture).
In the definition, it should mention what the canonical source is for this indicator.
Where there are formulas, include those as well.
Goals related to KPIs should co-exist with the definition.
For example, "Wider community contributions per release" should be in the Community Relations part of the handbook and "Average days to hire" should be in the Talent Acquisition part of the handbook.

## KPI Index

## Legend

The icons below are relevant for [Phase 1](/handbook/business-ops/data-team/kpi-index/#phase-1) and can be assigned by anyone at GitLab.

📊 indicates that the KPI is operational and is embedded in the handbook next to the definition and shown publicly (can be Sisense or another system).

🔗 indicates that the KPI is operational and there is a link from the handbook to Sisense, GitLab, Bitergia, Grafana, or another system.
This may be because the KPI cannot be public or because it isn't possible to embed it yet.

🚧 indicates that the KPI is in a `WIP: work in progress` status estimated to be shipped in any system within the month. When using this indicator, an issue should also be linked from this page.

🐔 indicates that the KPI is unlikely to be operationalized in the near term.

## GitLab KPIs

GitLab KPIs are duplicates of goals of the reports further down this page.
GitLab KPIs are the 10 most important indicators of company performance, and the most important KPI is IACV.
We review these at each quarterly meeting of the Board of Directors.
These KPIs are determined by a combination of their stand alone importance to the company and the amount of management focus devoted to improving the metric.

1. [Net ARR](https://about.gitlab.com/handbook/sales/sales-term-glossary/arr-in-practice/#net-arr) vs. [plan](https://about.gitlab.com/handbook/sales/performance-indicators/#net-arr-vs-plan) > 1 [🔗](https://app.periscopedata.com/app/gitlab/832223/Sales-KPI's?widget=11155197&udv=0) (lagging) **North Star KPI**
1. [R&D Overall MR Rate](/handbook/engineering/performance-indicators/#rd-overall-mr-rate) > 10 [📊](https://app.periscopedata.com/app/gitlab/710733/GitLab-Project-Efficiency?widget=9287585) (leading)
1. [Estimated Combined Monthly Active Users (CMAU)](/handbook/product/performance-indicators/#estimated-combined-monthly-active-users) > 17% QoQ [📊](https://app.periscopedata.com/app/gitlab/634200/Usage-Ping-SMAU-Dashboard?widget=9051075) (leading)
1. [Net New Business Pipeline Created ($s)](/handbook/marketing/performance-indicators/#net-new-business-pipeline-created) v Plan > 1 [📊](https://app.periscopedata.com/app/gitlab/431555/Marketing-Metrics?widget=6592070) (leading)
1. [Pipeline coverage start of quarter stage 3+](/handbook/marketing/performance-indicators/#pipeline-coverage) > 1.8 (leading)
1. [Percent of Ramped Reps at or Above Quota](/handbook/sales/performance-indicators/#percent-of-ramped-reps-at-or-above-quota) > 55% [📊](https://app.periscopedata.com/app/gitlab/832223/Sales-KPI's?widget=11433770&udv=0) (lagging)
1. [Growth Efficiency](/handbook/sales/#growth-efficiency) >= 1.5 by FY 23
1. [Net Retention](/handbook/customer-success/vision/#retention-and-reasons-for-churn) [> 145%](https://about.gitlab.com/handbook/sales/performance-indicators/#net-retention) [🔗](https://app.periscopedata.com/app/gitlab/832223/Sales-KPI's?widget=11155475&udv=0) (lagging)
1. [Gross Retention](/handbook/customer-success/vision/#retention-and-reasons-for-churn) [> 90%](https://about.gitlab.com/handbook/sales/performance-indicators/#gross-retention) [🔗](https://app.periscopedata.com/app/gitlab/403244/Retention?widget=5435598&udv=1067274) (lagging)
1. [12 Month Team Member Voluntary Retention](/handbook/people-group/people-success-performance-indicators/#team-member-voluntary-retention-rolling-12-months) > 90% [🔗](https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=9592672&udv=904340) (lagging)

## CoST KPIs

<%= kpi_list_by_org("Chief of Staff Team") %>

## Sales KPIs

<%= kpi_list_by_org("Sales") %>

## Marketing KPIs

<%= kpi_list_by_org("Marketing") %>

## People Group KPIs

### People Success KPIs

<%= kpi_list_by_org("People Success") %>

### Talent Acquisition KPIs

<%= kpi_list_by_org("Talent Acquisition") %>

## Finance KPIs

### Corporate Financial KPIs - Reported in FP&A Variance Package Monthly

<%= kpi_list_by_org("Corporate Finance") %>

### Finance Team KPIs - Reported in Key Review

<%= kpi_list_by_org("Finance Team") %>

## Product KPIs

<%= kpi_list_by_org("Product") %>

## Engineering KPIs

<%= kpi_list_by_org("Engineering Function") %>

### Customer Support Department KPIs

<%= kpi_list_by_org("Customer Support Department") %>

### Development Department KPIs

<%= kpi_list_by_org("Development Department") %>

### Infrastructure Department KPIs

<%= kpi_list_by_org("Infrastructure Department") %>

### Quality Department KPIs

<%= kpi_list_by_org("Quality Department") %>

### Security Department KPIs

<%= kpi_list_by_org("Security Department") %>

### UX Department KPIs

<%= kpi_list_by_org("UX Department") %>

## GitLab Metrics

We share a spreadsheet with investors called "GitLab Metrics."
The GitLab Metrics Sheet should be a subset of the KPIs listed on this page.
Alternatively, the sheet may show variations or subsets of one of those KPIs, such as showing all licensed users and then licensed users by product.

## Satisfaction

We do satisfaction scope on a scale of 1 to 5 how satisfied people are with the experience.
We don’t use NPS since that cuts off certain scores and we want to preserve fidelity.
We have the following abbreviation letter before SAT, please don’t use SAT without letter before to specify it:

- C = unused since customer is ambiguous (can mean product or support, not all users are customers)
- E = unused since employee is used by other companies but not by us
- I = [Interviewee](/handbook/hiring/metrics/#interviewee-satisfaction-isat) (would you recommend applying here)
- L = [Leadership](/handbook/eba/#leadership-sat-survey) (as an executive with dedicated administrative support, how is your executive administrative support received)
- O = [Onboarding](/handbook/people-group/people-operations-metrics/#onboarding-tsat) (how was your onboarding experience)
- P = [Product](/handbook/product/metrics/#paid-net-promoter-score) (would you recommend GitLab the product)
- S = [Support](/handbook/support/#support-satisfaction-ssat) (would you recommend our support followup)
- T = Team-members (would you recommend working here)

## Retention

Since we track retention in a lot of ways, we should never refer to just "Retention" without indicating what kind of retention.
We track:

- [Net Retention](/handbook/customer-success/vision/#retention-and-reasons-for-churn)
- [Gross Retention](/handbook/customer-success/vision/#retention-and-reasons-for-churn)
- User Retention
- [Team Member Retention](/handbook/people-group/people-group-metrics/#team-member-retention)

## Layers of KPIs

We have KPIs at many different [layers](/company/team/structure/#layers).

KPIs can only exist at the Company (e.g. GitLab) layer if it exists at the functional layer.
In other words, GitLab KPIs are duplicates of KPIs of the executives.
Not all functional KPIs are GitLab KPIs but all GitLab KPIs are functional KPIs.

As GitLab grows, this will also be true throughout the layers.
Not all departmental KPIs will be functional KPIs but all functional KPIs will be department KPIs.
This will cascade throughout the organization, as all job families will have performance indicators associated with them.

The [KPI Index](/handbook/business-ops/data-team/metrics/) captures the company, functional, and departmental KPIs since these are the three highest layers.

The only exception to this is where the filter on a KPI may change.
For example, the GitLab KPI may be "Hires vs Plan" but the Engineering KPI may be "Engineering Hires vs Plan".
The logic is the same, but the filter changes.

## Parts of a KPI

A KPI or metric consists of multiple things:

1. Definition: What is the data source? How is it calculated? What fields are included? What caveats are considered? Why is it chosen?
    - Note: Please see [Infrastructure Hosting Cost per MAU](/handbook/engineering/infrastructure/performance-indicators/#infrastructure-hosting-cost-per-gitlab-com-monthly-active-users) as an example.
1. Target: What we strive to be above, e.g. IACV has a target
1. Cap: What we strive to be below, e.g. Turnover has a cap
1. [Job family](/handbook/hiring/job-families/): link to job families with this as a performance indicator
1. Plan: what we have in our yearly plan
1. Commit: the most negative it will be
1. 50/50: the median estimate, 50% chance of being lower and higher
1. Best case: the most positive it will be
1. Forecast: what we use in our rolling 4 quarter forecast
1. Actual: what the number is

## What is public?

In the doc 'GitLab Metrics at IPO' are the KPIs that we may share publicly.
All KPIs have a public definition, goal, and job family links.
The actual performance an various estimates can be:

1. Live reported
1. Quarterly reported
1. Private
