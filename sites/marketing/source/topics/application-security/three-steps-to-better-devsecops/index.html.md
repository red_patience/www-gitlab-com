---
layout: markdown_page
title: "Three DevSecOps best practices to implement immediately"
description: With bad actors targeting code more than ever, it’s time to embrace the importance of DevSecOps. Here are three steps teams can take now to improve their application security.
---

No matter where you are on your DevOps journey, it’s time to lean into DevSecOps like never before. Why? Because the at-risk surfaces have never been larger. Today, applications are the single biggest security target, according to Forrester Research VP Amy DeMartine, who stressed [the problem is getting worse and not better](/blog/2020/10/14/why-security-champions/).

Complicating matters further, research from Gartner shows <a href="https://www.cybersecuritydive.com/news/security-budget-gartner/587911/" target="_blank">IT spending on security</a> is actually falling and represented just 5.7% of the total budget in 2019. 
And as if all that is not enough, there is ongoing confusion within DevOps teams about exactly which groups “own” the responsibility for security. In our 2020 Global DevSecOps Survey, 33% of security pros said they were solely responsible for security, but nearly as many – 29% – said everyone was. 

It’s time to rethink how teams approach DevSecOps, starting from the ground up. Here are three strategies teams can implement immediately.

1. Start with collaboration and communication

Collaboration is the key to success for every project. Bring together your project leaders and security delegates. Give them one meeting (with a pre-read or pre-plan) to devise a set of security measures that need to be fulfilled by code written for this project, and plan out the automated tests that developers will need to run on their code. Making these decisions together will foster both trust in the process and encourage buy-in to a security-by-design mentality.

<a href="https://securityintelligence.com/take-your-relationship-with-devsecops-to-the-next-level/" target="_blank">Rob Cuddy from IBM</a> advises your joint team adopt three important communication points to take your DevSecOps to the next level:
Communicate only serious issues - and filter out excess noise using AI and machine learning to verify your security scans.
Talk about the elephant in the room: Open source. Third party and open source code is omnipresent in software development, so it’s critical to address it directly to reduce the likelihood of preventable attacks.

Get to the root issues and deal with them faster: Find and fix false negatives before they’re exploited.

Take these steps to encourage direct, honest, and tactful communication across teams - it will help you build and maintain a level of trust and credibility critical to efficient and effective DevSecOps.

2. Write once, test often

Given the limited resources available for application security and its important role in business success, it only makes sense to run tests at every code commit. Ideally, these tests will be written once to meet project or organizational standards, and then run automatically against every code change. Focus tests on areas within the application that provide the most coverage but require minimal maintenance. Teams should analyze code from every structural level to look for issues affecting the operational performance of an app. The code should be safe, robust, efficient, and easy to maintain.

Preventative measures like [SAST}(https://docs.gitlab.com/ee/user/application_security/sast/) and [dependency scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) will save time in later phases by reducing the number of code defects before the code is merged and helping developers understand how changes will impact other areas of the application. Establishing test criteria first will also help developers improve the overall quality of their code by providing them with standards to refer to and achieve while writing code.

3. Use test results to educate, not punish

Applying test results as negative reinforcement is not a constructive practice. Beyond remediation, results can be leveraged in two ways:
The individual developer should use results as learnings on how to produce higher quality code.

At the group level, test results should be scanned for patterns in coding practices that can be improved upon, and used to create standards that will help improve code quality across the entire team or organization.

## Learn more about DevSecOps:

[How a security champions program](/blog/2020/10/14/why-security-champions/) can mean better DevSecOps

How GitLab [enables DevSecOps](/solutions/dev-sec-ops/)

Understand the [DevSecOps landscape](/developer-survey/)
